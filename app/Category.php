<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable =['category_id','product_id','created_at'];

    public function category(){
        return $this->hasMany('App\Category');
    }
}
