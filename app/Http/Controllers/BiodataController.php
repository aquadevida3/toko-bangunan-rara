<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biodata;


class BiodataController extends Controller
{
    public function index(){
        $biodata = Biodata::all();

        return view('biodata.index', compact('biodata'));

        // $biodata = Biodata::where('users_id', Auth::id())->first();
        // return view('biodata.index', compact('Biodata'));
    
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' =>'required',
            'no_hp' =>'required',
        ]);
        
        $biodata = Biodata::find($id);

        $biodata->name = $request['name'];
        $biodata->no_hp = $request['no_hp'];
        
        $biodata->save();
        
        return redirect ('/biodata');
    }

}
