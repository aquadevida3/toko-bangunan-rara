<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function pdf2(){
        $data = 'TES DOM PDF GAES FINAL PROJECT';
        $pdf = PDF::loadView('pdf.pdf', compact('data'));
        return $pdf->download('invoice.pdf');
    }
}
