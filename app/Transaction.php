<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = 'transaction';
    protected $fillable =['customer_id','product_id','created_at'];

    public function customer(){
        return $this->belongsTo('App\Customer');//belongs to ke model bernama Customer
    }

    public function product(){
        return $this->belongsTo('App\Product');//belongs to ke model bernama Product
    }
    
}
