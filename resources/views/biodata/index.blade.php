@extends('layout.admin_pluto')

@section('judul')
Halaman Update Biodata 
@endsection

@section('content')
@forelse ($biodata as $item)
<form action="/biodata/{{$item->id}}" method="POST">
@csrf
@method('PUT')

    

<div class="form-group">
    <label>Nama Admin</label>
    <input type="text" class="form-control" value="{{$item->name}}" disabled>
    </div>
    
    <div class="form-group"> 
    <label>No Handphone</label>
    <input type="number" class="form-control" name="no_hp" value="{{$item->no_hp}}">
    </div>
    @error('no_hp')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    @empty

    <tr colspan="3">
        <td>No data</td>
    </tr>
    

    @endforelse


{{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
 <a href="/" class="btn btn-primary">Back</a>

</form>

@endsection






