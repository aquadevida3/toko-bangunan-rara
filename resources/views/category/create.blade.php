@extends('layout.admin_pluto')

@section('judul')
Halaman Tambah Nama Kategori
@endsection

@section('content')

<form action="/category" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama_category" class="form-control">
    </div>
    @error('nama_category')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
       <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection