@extends('layout.admin_pluto')

@section('judul')
Halaman List Kategori
@endsection

@section('content')

<a href="/category/create" class="btn btn-primary my-2">Tambah</a>

<table class="table" id="example1">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Categories Name</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($category as $key=>$value) <!--jika datanya ada isinya maka tampilkan yang ini-->
            <tr>
                <td>{{$key + 1}}</th>   {{-- supaya nomor di depan tampil 12345 --}}
                <td>{{$value->nama_category}}</td> <!-- sesuaikan sama colom di database-->
                
                {{-- <td>
                       
                <ul>

                    @foreach ($value->category as $item)
                        <li>{{$item->nama_category}}</li>
                        
                    @endforeach

                </ul>
                </td> --}}
                <td>
                    <form action="/category/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/category/{{$value->id}}" class="btn btn-info">Detail</a>
                        <a href="/category/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" onclick="return confirm('Are You Sure?')" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty <!--jika datanya isinya kosong maka tampilkan yang ini-->
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
{{$category->links()}}

@endsection