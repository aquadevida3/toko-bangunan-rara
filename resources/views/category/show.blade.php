@extends('layout.admin_pluto')

@section('judul')
Halaman Detail Kategori {{$category->nama_category}}
@endsection

@section('content')

<h1>{{$category->nama_category}}</h1>

<a href="/category" class="btn btn-secondary">Kembali</a>

@endsection