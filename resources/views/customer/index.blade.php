@extends('layout.admin_pluto')

@section('judul')
LIST CUSTOMER  {{--INI BUAT JUDUL --}}
@endsection


@section('content')

<h2>List Customer</h2>
<br><br>
@auth
<a href="/customer/create" class="btn btn-success mb-2">Tambah</a>
@endauth



<table class="table ">

<thead class="thead-light">

<tr>

<th style="text-align:center" scope="col">No</th>

<th style="text-align:center" scope="col">Name</th>

<th style="text-align:center" scope="col">Email</th>

<th style="text-align:center" scope="col">Address</th>

<th style="text-align:center" scope="col">Photo</th>

<th style="text-align:center" scope="col" >Actions</th>

</tr>

</thead>

<tbody>
    @forelse ($customer as $key=>$value) 
        <tr>
            <td>{{$key + 1}}</th>   
            <td>{{$value->name}}</td> 
            <td>{{$value->email}}</td>
            <td>{{$value->address}}</td>
            <td><img height="90px" class="card-img-top" src="{{asset('photo_customer/'.$value->photo)}}" alt="Card image cap"></td>
            
            @auth
                    
            <td>
                
                <form action="/customer/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/customer/{{$value->id}}" class="btn btn-info">Detail</a>
                    <a href="/customer/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <input type="submit" onclick="return confirm('Are You Sure?')" class="btn btn-danger my-1" value="Delete">
                </form>

            </td>
            @endauth
        </tr>
    @empty 
        <tr colspan="3">
            <td>No data</td>
        </tr>  
    @endforelse
</tbody>

</table>
{{$customer->links()}}
    
@endsection