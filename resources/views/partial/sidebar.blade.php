<nav id="sidebar">
               <div class="sidebar_blog_1">
                  <div class="sidebar-header">
                     <div class="logo_section">
                        <a href="index.html"><img class="logo_icon img-responsive" src="{{asset('admin/images/logo/logo_icon.png')}}" alt="#" /></a>
                     </div>
                  </div>
                  <div class="sidebar_user_info">
                     <div class="icon_setting"></div>
                     <div class="user_profle_side">
                        <div class="user_img"><img class="img-responsive" src="{{asset('admin/images/layout_img/user_img.jpg')}}" alt="#" /></div>
                        <div class="user_info">
                           @guest
                           <h6>Belum Login</h6>
                           @endguest
                           @auth
                           {{ Auth::user()->name }}   
                           <p><span class="online_animation"></span> Online</p> 
                           @endauth
                           
                           
                        </div>
                     </div>
                  </div>
               </div>
               <div class="sidebar_blog_2">
                  <h4>Toko RARA</h4>
                  <ul class="list-unstyled components">
                     
                     <li><a href="/"><i class="fa fa-dashboard yellow_color"></i> <span>Dashboard</span></a></li>
                   
                     @auth
                  
                     <li>
                        <a href="/product"><i class="fa fa-table purple_color2"></i> <span>Product</span></a>
                     </li>
                     <li>
                        <a href="/category"><i class="fa fa-table purple_color2"></i> <span>Category</span></a>
                     </li>
                     <li>
                        <a href="/customer"  aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> <span>Customers</span></a>
                     </li>
                     <li>
                        <a href="/biodata"  aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> <span>Profile</span></a>
                     </li>
                     <li>
                        <a href="/transaction"  aria-expanded="false"><i class="fa fa-table purple_color1"></i> <span>Transaction</span></a>
                     </li>
                     <li>
                        <a href="/pdf2"  aria-expanded="false"><i class="fa fa-table purple_color2"></i> <span>DOM PDF</span></a>
                     </li>

                       {{-- ini untuk log out --}}
                     <li class="nav-item">
                        <a class="nav-link bg-danger" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{-- {{ __('Logout') }}  kita ganti versi kita--}}
                        
                        <i class='nav-icon fas fa-th'></i>
                        <b><p style="color:black">
                        Logout
                        </p></b>
                     </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>
                  </li>
                  @endauth
                  @guest
                  <li>
                  <a href="/login" class="nav-link bg-primary">
                  <i class='nav-icon fas fa-th'></i>
                  <p style="color:black"><b>
                  Login
                  </b></p>
                  </a>
                  </li>
                  @endguest

  
                  </ul>
               </div>
            </nav>