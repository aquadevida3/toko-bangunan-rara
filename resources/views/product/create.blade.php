@extends('layout.admin_pluto')

@section('judul')
HALAMAN INDEX   
@endsection

{{-- STACK STYLE DAN SCRIPT UNTUK LIBRARY SELECT 2 --}}
@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />  
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>  
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
    });
</script>

@endpush
{{-- SAMPAI SINI --}}

{{-- library tiny mce --}}
@push('script')
<script src="https://cdn.tiny.cloud/1/uk2mxtvj1fylr7xbwh1syguu8onyzd0vhcbsivjonobv9fwy/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>  

    <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>   
@endpush

{{-- sampe sini tiny mce --}}

@section('content')

<form action="/product" method="POST">
            @csrf
            <div class="form-group">
                <label>Product Name</label>
                <input type="text" class="form-control" name="name_product" placeholder="Masukkan Nama Product">
                @error('name_product')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input type="text" class="form-control" name="price" placeholder="Masukkan harga Product">
                @error('price')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea name="desc" class="form-control" cols="30" rows="5" placeholder="Masukkan Deskripsi Product"></textarea>
                @error('desc')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Stok</label>
                <input type="number" class="form-control" name="stock" placeholder="Masukkan Stock Barang">
                @error('stock')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Kategori</label><br>
                <select name="category_id" id="" class='js-example-basic-single'>
                    <option value="">--Pilih Kategori--</option>
                    @foreach ($category as $item)
                        <option value="{{$item->id}}">{{$item->nama_category}}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection
