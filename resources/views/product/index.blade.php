@extends('layout.admin_pluto')

@section('judul')
List Product 
@endsection

@section('content')
<a href="/product/create" class="btn btn-primary mb-3">Add Product</a>
<table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Product Name</th>
                <th scope="col">Price</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Stock</th>
                <th scope="col">Actions</th>
                <tbody>
                @forelse ($product as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name_product}}</td>
                        <td>{{$value->price}}</td>
                        <td>{{$value->desc}}</td>
                        <td>{{$value->stock}}</td>
                        <td>      
                            <form action="/product/{{$value->id}}" method="POST">
                            <a href="/product/{{$value->id}}" class="btn btn-info">Detail</a>
                            <a href="/product/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>

                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
              </tr>
            </thead>

            
        </table>

@endsection
