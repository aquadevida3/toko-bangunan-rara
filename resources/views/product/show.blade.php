@extends('layout.admin_pluto')

@section('judul')
Detail Product 
@endsection

@section('content')

<div class="card">
  <h3 class="card-header">{{$product->name_product}}</h3>
  <div class="card-body">
    <h5 class="card-title">Harga Product : {{$product->price}}</h5>
    <h5 class="card-title">Stock Product : {{$product->stock}}</h5>
    <p class="card-text">Deskripsi Product : {{$product->desc}}</p>
    <a href="/product" class="btn btn-primary">Kembali</a>
  </div>
  
</div>




@endsection