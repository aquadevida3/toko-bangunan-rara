@extends('layout.admin_pluto')

@section('judul')
Transaction
@endsection

@section('content')

<table class="table ">

    <thead class="thead-light">
    
    <tr>
    
    <th scope="col">#</th>
    
    <th scope="col">Customer Id</th>
    
    <th scope="col">Product Id</th>
    
    <th scope="col">Transaction Date</th>
    
    </tr>
    
    </thead>
    
    <tbody>
    
    //code disini tampilakan semua data di database dan buat link dan tombol untuk edit, detail, dan delete
          
                    @foreach ($transaction as $value) 
                        <tr>
                            {{-- <td>{{$key + 1}}</th>    --}}
                            <td>{{$value->customer}}</td> 
                            {{-- <td>{{$value->product_id}}</td>
                            <td>{{$value->created_at}}</td> --}}
                                
                            <td>
                                <form action="/transaction/{{$value->id}}" method="POST">
                                    @csrf
                                    
                                    {{-- <a href="/transaction/{{$value->id}}" class="btn btn-info">Detail</a> --}}
                                   
                                   
                                </form>
                            </td>
                        </tr>
                    @empty 
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse              
               
            
    
    </tbody>
    
    </table>
@endsection